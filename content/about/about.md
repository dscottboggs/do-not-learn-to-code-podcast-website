---
date: 2016-09-25T02:11:48-05:00
description: About the Do Not Learn To Code Podcast
title: About the Do Not Learn To Code Podcast
author: Scott
---

# Do **NOT** Learn To Code
It's bad for you. It doesn't make the world a better place. We have more important things to do.

This is a show for people disillusioned with the tech industry, whether it be through the way that
office work in general is harmful to the human body, the fact that it's a huge waste of resources,
or that it's an economic bubble propping up an inherently exploitative and ecologically harmful
economic system, or something else. We also want to focus on the ways in which technology *can* be
helpful, when applied in the service of and cooperation with the natural ecosystem. If you like
the sounds of that, subscribe! If you have a story to tell, we'd love to have you on as a guest!