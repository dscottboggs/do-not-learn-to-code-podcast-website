---
Title: D. Scott Boggs
date: 2021-07-25T15:12:37-04:00
Twitter: dscottboggs
# Website: "http://www.google.com/"
Type: "host"
# Facebook: fuck no
# Linkedin: fuck off
Pronouns: He/Him
GitHub: dscottboggs
Thumbnail: "img/host/scott.jpg"
YouTube: dscottboggs
# Twitch:
# Hide: ""
---

Scott has drifted in and out of the tech industry for his whole adult life. He first learned to code at 13, but dedicated himself deeply to specifically getting into software development at 25. By 27, it seemed certain that his expertise in the field was adequate, even if at the time it was undervalued. He had become well known within the company he worked as the guy who was always coding.

Then, the pandemic came, and the job he had went. Trying to find a new job seemed futile -- the only industry seeming to still be functioning was the war industry. No longer needing to put so much effort into learning to code, he had the time to reflect on the damage the past two years and more had done to him. Social connections had dried up, his mental health was deteriorated, and he'd gained a significant amount of weight and poor coping habits.

It wasn't until about a year after being laid off when these events form a cohesive concept to him -- the idea that, while coding itself isn't inherently harmful, the way in which our culture encourages us to learn and practice the trade *is*. He hopes to fully explore this idea in the podcast Do Not Learn To Code.