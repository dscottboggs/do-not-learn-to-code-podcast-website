---
Date: 2021-07-26T07:11:53-04:00
title: Owen Faison-Menzel
Pronouns: He/Him
Type: host
Twitter: catamountcommie
Thumbnail: img/host/owen.jpg
# Website:
# Facebook:
# Linkedin:
# GitHub:
# Thumbnail:
# Pinterest:
# Instagram:
# YouTube:
# Twitch:
---
Owen is a lifelong leftist. He says it's only because he's had the luxury and privilege to learn about leftism from a young age, but the truth is every adult around him has attempted to disabuse him of his rejection of capitalism from the start, to no avail. Growing up on food stamps in a poor tourist town adjacent to petit bourgeois family members within the incredible intergenerational dysfunction of a white, old-money WASP family has given him some small insights into class difference (as well as serious mental issues), and working an office job for years on end after dropping out of college (due to said mental issues) cemented his antagonism to the owner class (as well as the functional consequences of said mental issues).

He's got a few chips on his shoulder, but he means well. His current projects include developing a sustainable farm on his petit-bourg family's property to contribute to food sovereignty in the Catskills region and taking his meds on time.