---
date: 2016-09-25T02:11:48-05:00
description: guests of the show
title: Guests of Do Not Learn To Code
aliases: /guests
---

Want to be a guest on Do Not Learn To Code? Send us an email, tweet, discord message, whatever! If
privacy is a concern, I (Scott) can be reached via ProtonMail encrypted email at scott@tams.tech.
