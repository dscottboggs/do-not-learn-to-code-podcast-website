---
Date: 2021-08-07T12:06:46-04:00
title: Contact us
---
If you have a correction, comment, or question, we'd be glad to chat directly through the below mediums. You can also join our [Matrix group-chat here](https://matrix.to/#/#donotlearntocode:matrix.org).


Please note that *any **confidential** messages* should be sent to Scott via ProtonMail/OpenPGP-encrypted email or an encrypted Matrix chat using the below addresses.

---

### Owen

| Platform | Username |
| -------- | -------- |
| Twitter | [@catamountcommie](https://twitter.com/catamountcommie) |

### Scott

| Platform | Username |
| -------- | -------- |
| Matrix | [@dscottboggs:matrix.to](https://matrix.to/#/@dscottboggs:matrix.org) | |
| Email[^1] | scott.dnltc@tams.tech |
| Twitter | [@dscottboggs](https://twitter.com/dscottboggs) | |

[^1]: ProtonMail and OpenPGP encrypted email accepted. Fingerprint 401b295677c48ccdf2798401bc384d79a43c25ac
