---
Description: New Leftist Tech-News Podcast Seeks Co-Host
Date: 2021-07-24T15:04:20-04:00
PublishDate: 2021-07-24T15:04:20-04:00 # this is the datetime for the when the epsiode was published. This will default to Date if it is not set. Example is "2016-04-25T04:09:45-05:00"
title: New Leftist Tech-News Podcast Seeks Co-Host
blog_banner: img/episode/default-banner.jpg
blog_image: img/episode/default.jpg
# images: ["img/episode/default-social.jpg"]
Author: scott
# aliases: [ "/##" ]
# categories: [ a, list ]
# tags: [ a, list]


---

Hello, my name is Scott. I learned to code. I like to code, sometimes. It can be fun. But the endless drive for meaningless economic growth means that most of what gets "coded" these days is useless stuff meant to create jobs for the sole sake of creating jobs. Meaningful, valuable work (including software) that we all rely on is neglected, while massive portions of the economy depend on the success of "technology" which has no appreciable benefit to mankind as a whole, in support of an economic system which is rapidly destroying the planet.

If this sounds like a podcast you might be interested in hearing, hit the subscribe button. I'm planning on launching within the next month or so with bi-weekly episodes, with the goal of ramping up to weekly should time permit.

My partner Owen and I are co-hosts of the anti-capitalist recent-events radio program and podcast [The Truth Is So Boring](https://thetruthissoboring.org/), which airs every other week on WIOX 91.3FM in the Catskills region of New York. However, I feel that *this* podcast would turn out better if we had one or two other hosts who aren't as intimately familiar with us as we are with each other. Owen and I have been through many formative experiences together, and I think the end product would be better if we were discussing those experiences with someone with a unique perspective from our own.

To this end, **we seek additional co-hosts**. I think the pod would work best with a panel of 4, but I'm a little flexible on the specific format. Chat with me. I think it'd be also nice to have some guests too, so if you have a story you'd like to share, we'd like to interview you for an individual episode as well.

## Requirements
 - My personal mental health has suffered pretty severely due to burnout, so I really need someone who is available between the hours of 8AM and 3PM Eastern Time. I generally get up around 4-5AM to get myself ready to do some gardening before it gets hot outside, so by mid-afternoon I'm too tired for content production most days.
 - A decent-sounding condenser microphone is a **must**.
 - A recording environment mostly free from background noise and interruptions.

## Other shit
 - Owen and I both run Linux on all our computers and things will be easier for me if you do too. If you know how to use WSL or Docker on Mac then that probably works good enough. This isn't a hard requirement, it's more of just something to be aware of.
 - I plan to do double-ended recording.
 - IDK maybe we should have someone who's funny? Podcasts talking serious shit with a funny guy on seem to do well.

## How to reach us
 - Twitter
   - Scott: [@dscottboggs](https://twitter.com/dscottboggs)
   - Owen: [@catamountcommie](https://twitter.com/)
 - Protonmail or OpenPGP encrypted (or plain) email: [dnltc.cohost-search@tams.tech](mailto:dnltc.cohost-search@tams.tech)

   PGP fingerprint: 401b295677c48ccdf2798401bc384d79a43c25ac
 - Matrix: [@dscottboggs:matrix.org](https://matrix.to/#/@dscottboggs:matrix.org)